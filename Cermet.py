import argparse
import openpyxl

parser = argparse.ArgumentParser()
parser.add_argument('-V', dest='V', help='Vulnerabilities File')
parser.add_argument('-F', dest='F', help='Forms File')
parser.add_argument('-I', dest='I', help='Internal Asset Group')
parser.add_argument('-E', dest='E', help='External Asset Group')
args = parser.parse_args()


def report(org, data):
    with open("Report_{}.txt".format(org), 'a') as file:
        file.write(data)


def read_vulnerabilities(org):
    try:
        # Cache for vulnerabilities
        cache = {}

        # Open workbook
        wb = openpyxl.load_workbook(args.V)
        ws = wb.worksheets[0]

        for row in ws.iter_rows():
            if str(row[0].value) == "Vulnerability":
                print("Skipping first row due to headers")
                pass
            else:
                if row[9].value is "":
                    # TODO: Add function to look score up online
                    continue
                if (row[9].value >= 7.0) and (str(row[1].value).split(".")[0] not in cache):
                    # Exports data into json format.
                    cache[str(row[1].value).split(".")[0]] = {
                        "Name": str(row[0].value),
                        "ID": str(row[1].value).split(".")[0],
                        "Cause": row[4].value,
                        "Score": row[9].value,
                        "Asset_Group": row[10].value,
                        "Vulnerability ID": row[1].value,
                        "Grade": "Fail"}
                if (row[9].value >= 4.0) and (row[9].value <= 6.9) and (str(row[1].value).split(".")[0] not in cache):
                    # Exports data into json format.
                    cache[str(row[1].value).split(".")[0]] = {
                        "Name": row[0].value,
                        "ID": str(row[1].value).split(".")[0],
                        "Cause": row[4].value,
                        "Score": row[9].value,
                        "Asset_Group": row[10].value,
                        "Vulnerability ID": row[1].value,
                        "Grade": "Action Point"}

        fail = 0
        action_points = 0

        # Summary Report

        report(org, "\n\n## Report## \nFails:\t{}\nAction Points:\t{}\n".format(fail, action_points))

        # External

        # Secure Configuration
        report(org, "\n\n##External: {}##\n\n## Secure Configuration ##\n\n".format(args.E))
        for vuln in cache:
            if (cache[vuln]["Grade"] == "Action Point") and (args.E in (
                    cache[vuln]["Asset_Group"]) and ("Configuration" or
                                                     "Encryption" or
                                                     "Application Security" or
                                                     "Legacy System" in cache[vuln]["Cause"])):
                report(org, cache[vuln]["Name"] + "\n")
        # Patch Management
        report(org, "\n## Patch Management ##\n")
        if (cache[vuln]["Grade"] == "Action Point") and (args.E in (
                cache[vuln]["Asset_Group"]) and ("Patch Management" in cache[vuln]["Cause"])):
            report(org, cache[vuln]["Name"] + "\n")

        # Internal

        # Secure Configuration
        report(org, "\n\n##Internal: {}##\n\n## Secure Configuration ##\n\n".format(args.I))
        for vuln in cache:
            if (cache[vuln]["Grade"] == "Action Point") and (args.I in (
                    cache[vuln]["Asset_Group"]) and ("Configuration" or
                                                     "Encryption" or
                                                     "Application Security" or
                                                     "Legacy System" in cache[vuln]["Cause"])):
                report(org, cache[vuln]["Name"] + "\n")
        # Patch Management
        report(org, "\n## Patch Management ##\n")
        if (cache[vuln]["Grade"] == "Action Point") and (args.I in (
                cache[vuln]["Asset_Group"]) and ("Patch Management" in cache[vuln]["Cause"])):
            report(org, cache[vuln]["Name"] + "\n")

        # Detailed Reporting
        # External

        report(org, "\n\n##Detailed##\n\n##External: {}##\n\n## Fails ##\n".format(args.E))
        for vuln in cache:
            if (cache[vuln]["Grade"] == "Fail") and (
                    args.E in (cache[vuln]["Asset_Group"])):
                fail += 1
                report(org, "{}\t{}\t{}\n".format(cache[vuln]["ID"], cache[vuln]["Name"], cache[vuln]["Score"]))
        report(org, "\n## Action Points ##\n")
        for vuln in cache:
            if (cache[vuln]["Grade"] == "Action Point") and (
                    args.E in (cache[vuln]["Asset_Group"])):
                action_points += 1
                report(org, "{}\t{}\t{}\n".format(cache[vuln]["ID"], cache[vuln]["Name"], cache[vuln]["Score"]))

        # Internal

        report(org, "\n##Internal: {}##\n\n## Fails ##\n".format(args.I))
        for vuln in cache:
            if (cache[vuln]["Grade"] == "Fail") and (args.I in
                                                     cache[vuln]["Asset_Group"]):
                fail += 1
                report(org, "{}\t{}\t{}\n".format(cache[vuln]["ID"], cache[vuln]["Name"], cache[vuln]["Score"]))
        report(org, "\n## Action Points ##\n")
        for vuln in cache:
            if (cache[vuln]["Grade"] == "Action Point") and (args.I in
                                                             cache[vuln]["Asset_Group"]):
                action_points += 1
                report(org, "{}\t{}\t{}\n".format(cache[vuln]["ID"], cache[vuln]["Name"], cache[vuln]["Score"]))

    except TypeError as e:
        print("Error: {}".format(e.args))
    except Exception as e:
        print(e.args)


def read_form(file):
    wb = openpyxl.load_workbook(file)
    ws = wb.worksheets[0]

    address = ""
    sector = ""
    employees = ""
    contact = ""
    email = ""
    role = ""
    scope = ""
    assessor = ""
    qa = ""
    level = ""
    start_date = ""
    end_date = ""
    reassessment = ""
    published = ""

    for row in ws.iter_rows():
        if str(row[8].value) == "Name of Organisation":
            org = str(row[9].value)
        if str(row[8].value) == "Registered Address":
            address = str(row[9].value)
        if str(row[8].value) == "Sector":
            sector = str(row[9].value)
        if str(row[8].value) == "Number of Employees":
            employees = str(row[9].value).split(".")[0]
        if str(row[8].value) == "Name of main contact":
            contact = str(row[9].value)
        if str(row[8].value) == "Contact Job title":
            role = str(row[9].value)
        if str(row[8].value) == "Contact Email":
            email = str(row[9].value)
        if str(row[8].value) == "CE Level Desired":
            level = str(row[9].value)
        if str(row[8].value) == "Can CREST publicise your successful certification?":
            published = str(row[9].value)

    scope = input("What was the scope?: ")
    assessor = input("Who did the assessment?: ")
    qa = input("Who did the QA?: ")
    start_date = input("When did the assessment start?: ")
    end_date = input("When did the assessment finish?: ")
    reassessment = input("Is this a new assessment or a recert?: ")

    data = "Details For Certificate\n" \
           "Org:\t{}\n" \
           "Address:\t{}\n" \
           "Sector:\t{}\n" \
           "Employees:\t{}\n" \
           "Contact:\t{}\n" \
           "Job Title:\t{}\n" \
           "Email Address:\t{}\n" \
           "CE level:\t{}\n" \
           "Scope:\t{}\n" \
           "Assessor:\t{}\n" \
           "QA:\t{}\n" \
           "Start Date:\t{}\n" \
           "End Date:\t{}\n" \
           "Reassessment:\t{}\n" \
           "Published by CREST:\t{}\n".format(org,
                                              address,
                                              sector,
                                              employees,
                                              contact,
                                              role,
                                              email,
                                              level,
                                              scope,
                                              assessor,
                                              qa,
                                              start_date,
                                              end_date,
                                              reassessment,
                                              published)
    report(org, data)
    read_vulnerabilities(org)


if __name__ == "__main__":
    print("Cyber Essentials Report Made easier... technically")
    if args.F:
        read_form(args.F)
    else:
        parser.print_help()
