# Cermet (Work In Progress)

# What is Cermet?

Cermet stands for; 
**C**yber **E**ssentials **R**eporting **M**ade **E**asier **T**echnically

# What does Cermet do?

The idea behind Cermet is to read the quesitonnaire and vulnerabilities file in order to assist with the information on reporting and certificate delivery.

Questionnaire: Cermet will extract the information required in order to assist in generating CE/CE+ certificates, such as Organisation name, Sector, Contact details, etc.

Vulnerabilies: Cermet will read all the vulnerabilities in the spreadsheet, determine whether they fit in External or Internal assets and whether they're sectioned into "Secure Configuration", "Patch Management", etc.


# Requirements

*  Openpyxl

# Usage

`Cermet.py -F Forms.xlsx -V Vulnerabilities.xlsx -I "Cyber Essentials Internal" -E "Cyber Essentials External"`

# TODO

*  Add functinality to lookup CVSSv3 scores if a score is not present within the file.
*  Clean up the output
*  Only output what's needed, I.e if it's just a CE, don't output anything for internal
*  Optimize code (I can dream...)